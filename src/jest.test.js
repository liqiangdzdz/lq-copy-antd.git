// 每个用例测试单独的功能点
test("test", () => {
    // toBe完全相同
    // toEqual 值相同
    expect(2+2).toBe(4)
    expect(2+2).not.toBe(5)
    expect(1).toBeTruthy()
    expect(0).toBeFalsy()
    expect(4).toBeGreaterThan(3)
    expect(4).toBeLessThan(5)
    expect({name:'lq'}).toEqual({name:'lq'})
});


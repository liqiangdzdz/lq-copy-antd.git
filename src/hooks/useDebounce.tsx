import React, { useEffect, useState } from "react";

function useDebounce(value: string, dalay = 300) {
    const [debounceValue, setDebounceValue] = useState(value)

    useEffect(() => {        
        const timer = setTimeout(() => {
            setDebounceValue(value)
        }, dalay);
        return () => {
            clearTimeout(timer)
        }
    }, [value, dalay])

    return debounceValue
}

export default useDebounce
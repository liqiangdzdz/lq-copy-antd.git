import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { Button } from './Button'

const defaultButton = () => (
    <Button onClick={action('clicked')}> default button </Button>
)
const sizeButton = () => (
    <>
        <Button size='lg' >large button</Button>
        <Button size='sm' >small button</Button>
    </>
)

const typeButton = () => (
    <>
        <Button btnType='primary'>primary</Button>
        <Button btnType='danger'>danger</Button>
        <Button btnType='link' href='https://www.baidu.com/' target={'_blank'}>link</Button>
        <Button disabled>disabled</Button>
    </>
)

storiesOf('Button 按钮', module)
    .add('Button', defaultButton)
    .add('不同尺寸的Button', sizeButton)
    .add('不同类型的Button', typeButton)


    // addDecorator:装饰器，可以反回一个节点
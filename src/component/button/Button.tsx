import React, { ReactNode } from "react";
import classNames from "classnames";
// 定义button按钮的接收值
// export enum ButtonSize {
//     Large = 'lg',
//     Small = 'sm'
// }

// export enum ButtonType {
//     Primary = 'primary',
//     Default = 'default',
//     Danger = 'danger',
//     Link = 'link'
// }
export type ButtonSize = 'lg' | 'sm'
export type ButtonType = 'primary' | 'default' | 'danger' | 'link'
interface IProps {
    // 定义button的接收值
    className?: string
    /** 设置button的禁用*/
    disabled?: boolean
    /** btnType为link时有效，button的的链接*/
    href?: string,
    /** 设置button的的大小*/
    size?: ButtonSize
    /** 设置button的的种类*/
    btnType?: ButtonType
    children?: ReactNode
}


// button的原生属性
type btnProps = IProps & React.ButtonHTMLAttributes<HTMLElement>
type aProps = IProps & React.AnchorHTMLAttributes<HTMLElement>
// Partial 全部设成？
export type IBtnProps = Partial<btnProps & aProps>

/**
 * 
 * 页面中最常用的组件，适用于完成特定的交互
 * ### 引用方法
 * ~~~js
 * import { Button } from './component/button/Button';
 * ~~~
 */
export const Button: React.FC<IBtnProps> = (props: IBtnProps) => {
    const { className, disabled, size, btnType, children, href, ...restProps } = props
    const btnClasses = classNames('btn', className, {
        [`btn-${btnType}`]: btnType,
        [`btn-${size}`]: size,
        'disable': (btnType === 'link') && disabled
    })
    if (btnType === 'link' && href) {
        return (
            <>
                <a className={btnClasses} href={href} {...restProps}>{children}</a>
            </>
        )
    }
    return (
        <>
            <button className={btnClasses} disabled={disabled} {...restProps}>{children}</button>
        </>
    )
}
Button.defaultProps = {
    disabled: false,
    btnType: 'default'
}
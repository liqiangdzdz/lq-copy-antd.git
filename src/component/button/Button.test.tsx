import React from "react";
import { fireEvent, render } from "@testing-library/react";
import { Button, IBtnProps, ButtonSize, ButtonType } from "./Button";

const defaultProps = {
    onClick: jest.fn()
}
const testProps: IBtnProps = {
    btnType:'primary',
    size: 'lg',
    className: 'klass'
}

const disabledProps: IBtnProps = {
    disabled: true,
    onClick: jest.fn(),
}
// test("btn", () => {
//     const primaryBtn = render(<Button>btn</Button>)
//     const element = primaryBtn.queryByText('btn')
//     expect(element).toBeInTheDocument()
// })
// 
// 分类
describe('test btn component', () => {
    // it等同于test都是一个case
    it('default btn', () => {
        const defaultBtn = render(<Button>Button</Button>)
        const e = defaultBtn.getByText('Button')
        expect(e.tagName).toEqual('BUTTON')
        expect(e).toHaveClass('btn btn-default')
        fireEvent.click(e)
        expect(defaultProps.onClick).toHaveBeenCalled
    })
    it('btn props', () => {
        const wrapper = render(<Button {...testProps}>Nice</Button>)
        const element = wrapper.getByText('Nice')
        expect(element).toBeInTheDocument()
        expect(element).toHaveClass('btn-primary btn-lg klass')
    })
    it('link btn', () => {
        const wrapper = render(<Button btnType={'link'} href="http://www.baidu.com">Link</Button>)
        const element = wrapper.getByText('Link')
        expect(element).toBeInTheDocument()
        expect(element.tagName).toEqual('A')
        expect(element).toHaveClass('btn btn-link')
    })
    it('disabled btn', () => {
        const wrapper = render(<Button {...disabledProps}>Nice</Button>)
        const element = wrapper.getByText('Nice') as HTMLButtonElement
        expect(element).toBeInTheDocument()
        expect(element.disabled).toBeTruthy()
        fireEvent.click(element)
        expect(disabledProps.onClick).not.toHaveBeenCalled()
    })
})
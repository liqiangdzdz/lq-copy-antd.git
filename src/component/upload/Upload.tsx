import axios from "axios";
import React, { ChangeEvent, FC, ReactNode, useRef, useState } from "react";
import { Button } from "../button/Button";
import { Progress } from "../progress/Progress";
import { UploadDragger } from "./UploadDragger";
import { UploadList } from "./UploadList";

export interface IUploadProps {
    /** 文件上传地址*/
    action: string;
    /**默认显示长传文件的信息 
     * ### uploadFile参数: 
     * 文件的ID uid:string;
     * 文件的字节 size:number;
     * 文件名 name:string;
     * 文件状态 status?:'ready' | 'uploading' | 'success' | 'error';
     * 文件上传进度 percent?:number;
     * 文件 raw?:File;
     * 成功信息 response?:any;
     * 失败信息 error?:any*/
    defaultFileList?: uploadFile[]
    /** 文件上传之前的回调函数,用户可以自行验证文件，反回布尔或者Promise类型的File*/
    beforeUpload?: (file: File) => boolean | Promise<File>
    /** 文件上传状态的回调函数*/
    onProgress?: (percentage: number, file: File) => void
    /** 文件上传成功的回调函数 */
    onSuccess?: (data: any, file: File) => void
    /** 文件上传失败的回调函数 */
    onError?: (err: any, file: File) => void
    /**文件状态改变时的回调函数 */
    onChange?: (file: File) => void
    /** 点击删除文件提示信息时触发的回调函数*/
    onRemove?: (file: uploadFile) => void
    /** 自定义Http请求头*/
    HttpHeader?: { [key: string]: any }
    /** 自定义上传文件名*/
    fileName?: string
    /** 自定义请求时的formData*/
    formData?: { [key: string]: any }
    /** 发送请求时是否携带cookie-withCredentials */
    withCredentials?: boolean
    /** 规定能够通过文件上传进行提交的文件类型*/
    accept?: string
    /** 规定输入字段可选择多个值*/
    multiple?: boolean,
    /** 是否可以拖动上传*/
    drag?: boolean
    children: React.ReactNode
}
export type uploadFileStatus = 'ready' | 'uploading' | 'success' | 'error'
export interface uploadFile {
    uid: string;
    // 文件的字节
    size: number;
    // 文件名
    name: string;
    // 文件状态
    status?: uploadFileStatus;
    // 文件上传进度
    percent?: number;
    // 文件
    raw?: File;
    // 成功信息
    response?: any;
    // 失败信息
    error?: any
}

export const Upload: FC<IUploadProps> = (props: IUploadProps) => {
    const { children, drag, action, onError, onProgress, onSuccess, beforeUpload, onChange, defaultFileList, onRemove, HttpHeader, fileName, formData, withCredentials, accept, multiple } = props
    const fileInputRef = useRef<HTMLInputElement>(null)
    const [fileList, setFileList] = useState<uploadFile[]>(defaultFileList || [])

    const handleFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
        const files = e.target.files
        if (!files) {
            return
        }
        uploadFiles(files)
        // 结束后清空
        if (fileInputRef.current) {
            fileInputRef.current.value = ''
        }
    }

    const uploadFiles = (files: FileList) => {
        let postFiles = Array.from(files)
        postFiles.forEach(async file => {
            if (!beforeUpload) {
                post(file)
            } else {
                const res = beforeUpload(file)
                if (res && res instanceof Promise) {
                    const data = await res
                    post(data)
                } else if (res !== false) {
                    post(file)
                }
            }
        })
    }

    const updateFileList = (updateFile: uploadFile, updateObj: Partial<uploadFile>) => {
        setFileList(item => {
            return item.map(i => {
                if (i.uid === updateFile.uid) {
                    return { ...i, ...updateObj }
                } else {
                    return i
                }
            })
        })
    }

    const post = async (file: File) => {
        // 这是存储文件信息
        let _file: uploadFile = {
            uid: 'upload-file-uid:' + Date.now(),
            status: 'ready',
            name: fileName || file.name,
            size: file.size,
            percent: 0,
            raw: file
        }
        // setFileList([_file, ...fileList])
        setFileList(item => {
            return [_file, ...item]
        })
        const formDatas = new FormData()
        formDatas.append(fileName || file.name, file)
        if (formData) {
            Object.keys(formData).map(i => {
                formDatas.append(i, formData[i])
            })
        }

        try {
            const res = await axios.post(action, formDatas, {
                headers: { ...HttpHeader, 'Content-Type': 'multipart/form-data' },
                withCredentials,
                onUploadProgress: (e) => {
                    let per = Math.round((e.loaded * 100) / e.total) || 0;
                    if (per < 100) {
                        updateFileList(_file, { percent: per, status: 'uploading' })
                        if (onProgress) {
                            onProgress(per, file)
                        }
                        if (onChange) {
                            onChange(file)
                        }
                    }
                }

            })
            console.log('success:', res);
            updateFileList(_file, { response: res.data, status: 'success' })
            if (onSuccess) {
                onSuccess(res, file)
            }
        } catch (error) {
            console.log('error:', error);
            updateFileList(_file, { error, status: 'error' })
            if (onError) {
                onError(error, file)
            }
            if (onChange) {
                onChange(file)
            }
        }
    }

    const handleClick = () => {
        if (fileInputRef.current) {
            fileInputRef.current.click()
        }
    }

    const handleRemove = (file: uploadFile) => {
        setFileList((item) => {
            return item.filter(i => i.uid !== file.uid)
        })
        if (onRemove) {
            onRemove(file)
        }
    }

    return (
        <>
            <div className="upload-component" onClick={handleClick}>
                <input
                    ref={fileInputRef}
                    type={'file'}
                    name="myfile"
                    className='file-input'
                    onChange={handleFileChange}
                    style={{ display: 'none' }}
                    accept={accept}
                    multiple={multiple}
                ></input>
                {drag ?
                    <UploadDragger onFile={(files) => { uploadFiles(files) }}>
                        {children}
                    </UploadDragger> :
                    children
                }
                <UploadList fileList={fileList} onRemove={handleRemove}></UploadList>
            </div>
        </>
    )
}

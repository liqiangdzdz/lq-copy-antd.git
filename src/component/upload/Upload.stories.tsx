import { action } from "@storybook/addon-actions";
import { storiesOf } from "@storybook/react";
import React from "react";
import { Button } from "../button/Button";
import { Icon } from "../icon/Icon";
import { Upload, uploadFile } from "./Upload";

// let fileListDate:uploadFile[] = [
//     { uid: '123', size: 1234, name: 'hello.md', status: 'uploading', percent: 30 },
//     { uid: '122', size: 1234, name: 'xyz.md', status: 'success', percent: 30 },
//     { uid: '121', size: 1234, name: 'eyiha.md', status: 'error', percent: 30 }
// ]

const upl = () => {
    return (
        <div style={{ width: '200px' }}>
            <Upload
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                onError={action('error:')}
                onProgress={action('progress:')}
                onSuccess={action('success:')}
                onChange={action('change:')}
                beforeUpload={filePromise}
                // defaultFileList={fileListDate}
                onRemove={action('remove')}
                // fileName='myFirstFileName'
                formData={{ 'keya': 'val' }}
                HttpHeader={{ 'X-Powered-By': 'myFirstFileName' }}
                multiple={true}
                // accept={'.png'}
            >
               <Button btnType="primary">uploader</Button>
            </Upload>
        </div>
    )
}
const dupl = () => {
    return (
        <div style={{ width: '200px' }}>
            <Upload
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                onError={action('error:')}
                onProgress={action('progress:')}
                onSuccess={action('success:')}
                onChange={action('change:')}
                beforeUpload={filePromise}
                // defaultFileList={fileListDate}
                onRemove={action('remove')}
                // fileName='myFirstFileName'
                formData={{ 'keya': 'val' }}
                HttpHeader={{ 'X-Powered-By': 'myFirstFileName' }}
                multiple={true}
                // accept={'.png'}
                drag={true}
            >
                <Icon icon={'upload'} size="5x" theme="secondary"></Icon>
                <br />
                <p>Drag file over to upload</p>
            </Upload>
        </div>
    )
}

const befupload = (file: File) => {
    if (Math.round(file.size / 1024) > 50) {
        alert('大于50k不处理')
        return false
    }
    return true
}

const filePromise = (file: File) => {
    const newFile = new File([file], 'new_name.docx', { type: file.type })
    return Promise.resolve(newFile)
}

storiesOf('upload 文件上传', module)
    .add('Upload', upl)
    .add('拖拽 Upload', dupl)
import React, { FC } from "react";
import { Icon } from "../icon/Icon";
import { Progress } from "../progress/Progress";
import { uploadFile } from "./Upload";

interface IUploadListProps {
    fileList: uploadFile[];
    onRemove: (_file: uploadFile) => void;
}

export const UploadList: FC<IUploadListProps> = (props: IUploadListProps) => {
    const { fileList, onRemove } = props
    return (
        <>
            <ul className="upload-list">
                {
                    fileList.map(item => {
                        return (
                            <li className="upload-list-item" key={item.uid}>
                                <span className={`file-name file-name-${item.status}`}>
                                    <Icon icon={"file-alt"} theme='secondary'></Icon>
                                    {item.name}
                                </span>
                                <span className="file-status">
                                    {(item.status === 'uploading' || item.status === 'ready') && <Icon icon="spinner" spin theme="primary" />}
                                    {item.status === 'success' && <Icon icon="check-circle" theme="success" />}
                                    {item.status === 'error' && <Icon icon="times-circle" theme="danger" />}
                                </span>
                                <span className="file-actions">
                                    <Icon icon={'times'} onClick={() => {onRemove(item)}}></Icon>
                                </span>
                                {
                                    item.status === 'uploading' && <Progress percent={item.percent || 0} storkeHeight={15}></Progress>
                                }·
                            </li>
                        )
                    })
                }
            </ul>
        </>
    )
}
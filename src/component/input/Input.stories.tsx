import { storiesOf } from "@storybook/react";
import React, { useState } from "react";
import { Input } from "./Input";

const ControllInput = (props: any) => {
    // 使用useState时必须写值，不然会触发react警告
    const [val, setVal] = useState('受控组件,具体得看autoComplete组件')
    return <Input value={val} onChange={(e) => { setVal(e.target.value);props.onCallBack(val)}} ></Input>
}

const input = () => (
    <div style={{ width: '600px' }}>
        <Input></Input>
        <h5>受控组件</h5>
        <ControllInput onCallBack={(val:any) => {
            console.log(val);
        }}></ControllInput>
    </div>
)

const disinput = () => (
    <div style={{ width: '600px' }}>
        <Input disabled={true}></Input>
    </div>
)
const sminput = () => (
    <>
        <div style={{ width: '600px' }}>
            <Input size="lg"></Input>
        </div>
        <div style={{ width: '600px' }}>
            <Input size="sm"></Input>
        </div>
    </>
)

const pinp = () => (
    <div style={{ width: '600px' }}>
        <Input prepand={'http://'}></Input>
    </div>
)
const ainp = () => (
    <div style={{ width: '600px' }}>
        <Input append={'.com'}></Input>
    </div>
)
const iinp = () => (
    <div style={{ width: '600px' }}>
        <Input icon={'search'}></Input>
    </div>
)

storiesOf('Input 搜索框', module)
    .add('Input', input)
    .add('禁用 input', disinput)
    .add('input 尺寸', sminput)
    .add('input 前置', pinp)
    .add('input 后置', ainp)
    .add('input 图标', iinp)
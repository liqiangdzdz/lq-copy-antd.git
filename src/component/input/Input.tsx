import { IconProp } from "@fortawesome/fontawesome-svg-core";
import classNames from "classnames";
import React, { ChangeEvent, InputHTMLAttributes, ReactElement, ReactNode } from "react";
import { Icon } from "../icon/Icon";

type InputSize = 'lg' | 'sm'

// Omit忽略冲突的值
export interface IInputProps extends Omit<InputHTMLAttributes<HTMLElement>, 'size'> {
    /** 禁用*/ 
    disabled?: boolean;
    /** 尺寸*/ 
    size?: InputSize;
    /** 图标*/ 
    icon?: IconProp;
    /** 前置*/ 
    prepand?: string | ReactElement;
    /** 后置*/  
    append?: string | ReactElement;
    children?: ReactNode;
    className?: string;
    style?:React.CSSProperties;
    onChange?:(e:ChangeEvent<HTMLInputElement>) => void
}

/**
 * 
 * 通过鼠标或键盘输入字符
 * ### 引用方法
 * ~~~js
 * import { Input } from './component/form/Input';
 * ~~~
 */

export const Input: React.FC<IInputProps> = (props: IInputProps) => {
    // 属性
    const { disabled, size, icon, prepand, append, children, className,style, ...restProps } = props
    // className
    const inputClassNames = classNames('input-wrapper', className, {
        [`input-size-${size}`]: size,
        'is-readonlay': disabled,
        'input-group': prepand || append,

    })

    const restrictValue = (val:any) => {
        if(typeof val === 'undefined' || typeof val === null){
            return ''
        }
        return val
    }

    if('value' in props){
        delete restProps.defaultValue
        restProps.value = restrictValue(restProps.value)
    }
    return (
        // 根据属性判断是否要添加特定的节点
        <>
            <div className={inputClassNames} style={style}>
                {prepand && <div className="input-group-prepend">{prepand}</div>}
                {icon && <div className="icon-wrapper"><Icon icon={icon} title={`title-${icon}`} /></div>}
                <input
                    className="input-inner"
                    disabled={disabled}
                    {...restProps}
                />
                {append && <div className="input-group-append">{append}</div>}
            </div>
        </>
    )
}
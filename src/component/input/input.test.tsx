import { fireEvent, render, RenderResult } from "@testing-library/react";
import React, { ReactElement } from "react";
import { IInputProps, Input } from "./Input";

const inputProps: IInputProps = {
    // 这是占位符
    placeholder: "test-input",
    onChange: jest.fn()
}

describe('input 测试', () => {
    it('默认输入', () => {
        let defaultWrapper: RenderResult = render(<Input {...inputProps}></Input>)
        // 此处拿到的是input
        let defaultElement: HTMLInputElement = defaultWrapper.getByPlaceholderText('test-input') as HTMLInputElement
        // input是否存在文档中
        expect(defaultElement).toBeInTheDocument()
        // 对应的className
        expect(defaultElement).toHaveClass('input-inner')
        // disabled
        fireEvent.change(defaultElement, { target: { value: '123' } })
        // 希望上面的方法被调用
        expect(inputProps.onChange).toHaveBeenCalled()
        expect(defaultElement.value).toEqual('123')
    });
    it('禁用状态', () => {
        const disabledWrapper = render(<Input disabled placeholder="disabled" />)
        const disabledElement = disabledWrapper.getByPlaceholderText('disabled') as HTMLInputElement
        expect(disabledElement.disabled).toBeTruthy()
    })
    it('尺寸', () => {
        const sizeLgWrapper = render(<Input placeholder="sizes" size="lg" />)
        const sizeLgElement = sizeLgWrapper.container.querySelector('.input-wrapper')
        expect(sizeLgElement).toHaveClass('input-size-lg')
        const sizeSmWrapper = render(<Input placeholder="sizes" size="sm" />)
        const sizeSmElement = sizeSmWrapper.container.querySelector('.input-wrapper')
        expect(sizeSmElement).toHaveClass('input-size-sm')
    })
    it('前后btn', () => {
        const { queryByText, container } = render(<Input placeholder="pend" prepand="https://" append=".com" />)
        const testContainer = container.querySelector('.input-wrapper')        
        expect(testContainer).toHaveClass('input-group')
        expect(queryByText('https://')).toBeInTheDocument()
        expect(queryByText('.com')).toBeInTheDocument()
    })
}) 
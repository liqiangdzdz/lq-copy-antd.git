import React, { FC } from "react";
import { ThemeProps } from "../icon/Icon";

export interface IProgressProps {
    /** 进度*/ 
    percent:number;
    /** 进度条的高度*/ 
    storkeHeight?:number;
    /**是否显示文字 */
    showText?:boolean;
     /**选择icon种类 */
     theme?:ThemeProps;
     style?:React.CSSProperties;
}
/**
 * 在操作需要较长时间才能完成时，为用户显示该操作的当前进度和状态。
 * 
 * ### 引用方法
 * 
 * ~~~js
 * import { Progress } from "./Progress";
 * ~~~
*/
export const Progress: FC<IProgressProps> = (props: IProgressProps) => {
    const {percent,storkeHeight,showText,theme,style} = props
    return (
        <div className="progress-bar" style={style}>
            <div className="progress-bar-outer" style={{height:`${storkeHeight}px`}}>
                <div className={`progress-bar-inner color-${theme}`} style={{width:`${percent}%`,height:`${storkeHeight}px`}}>
                    {showText && <span className="inner-text">{`${percent}%`}</span>}
                </div>
            </div>
        </div>
    )
}
Progress.defaultProps = {
    storkeHeight:30,
    showText:true,
    theme:'primary'
}
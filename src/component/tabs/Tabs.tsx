import classNames from "classnames";
import React, { createContext, ReactNode, useState } from "react";
import { ITabItemProps } from "./TabItem";

type TabsCallBack = (index: string) => void

export interface ITabsProps {
    /** 高亮*/
    defaltIndex?: string;
    /** 回调*/
    onSelect?: TabsCallBack;
    className?: string;
    style?: React.CSSProperties;
    children?: ReactNode
}

interface ITabsContext {
    defaltIndex: string;
    tabsOnSelect?: (index: string, tabItemChildren: ReactNode) => void
}

export const tabsContext = createContext<ITabsContext>({ defaltIndex: '0' })

/**
 *选项卡切换组件。 
 *### 引用方法
 * ~~~js
 *import { TabItem } from "./TabItem";
 *import { Tabs } from "./Tabs";
 * ~~~
 */
export const Tabs: React.FC<ITabsProps> = (props: ITabsProps) => {
    const { defaltIndex, onSelect, className, style, children } = props
    const [tabsIndex, setTabsIndex] = useState(defaltIndex)
    const [tabItemChildren, setTabItemChildren] = useState<ReactNode>()
    const tabsClassNames = classNames('tabs', className)
    const tabsOnSelect = (index: string, tabItemChildren: ReactNode) => {
        setTabsIndex(index)
        setTabItemChildren(tabItemChildren)
        if (onSelect) {
            onSelect(index ? index : '0')
        }
    }
    const tabsContextValue: ITabsContext = {
        defaltIndex: tabsIndex ? tabsIndex : '0',
        tabsOnSelect
    }

    // 限制类型
    const renderChildren = () => {
        return React.Children.map(children, (item, i) => {
            const child = item as React.FunctionComponentElement<ITabItemProps>
            if (child.type.displayName === 'TabItem') {
                return React.cloneElement(child, {
                    index: `${i}`
                })
            }
        })
    }
    return (
        <>
            {
                <tabsContext.Provider value={tabsContextValue}>
                    <ul className={tabsClassNames} style={style} data-testid="test-tabs">
                        {renderChildren()}
                    </ul>
                    <div className="tabs-cont">
                        {
                            tabItemChildren
                        }
                    </div>
                </tabsContext.Provider>
            }
        </>
    )
}

Tabs.defaultProps = {
    defaltIndex: '0'
}

import { FC } from "react";
import { ITabsProps, Tabs } from "./Tabs";
import { ITabItemProps, TabItem } from "./TabItem";

type ITabsType = FC<ITabsProps> & {
    item:FC<ITabItemProps>
}

const TransTabs = Tabs as ITabsType

TransTabs.item = TabItem

export default TransTabs



import { fireEvent, render, RenderResult } from "@testing-library/react";
import React from "react";
import { TabItem } from "./TabItem";
import { ITabsProps, Tabs } from "./Tabs";

const testTabsProps: ITabsProps = {
    defaltIndex: '0',
    onSelect: jest.fn(),
    className: 'testtabs'
}

const generateTabs = (props: ITabsProps) => {
    return (
        <>
            <Tabs {...testTabsProps}>
                <TabItem title='tab1'>tab1</TabItem>
                <TabItem disabled={true} title='tab2'>tab2</TabItem>
                <TabItem title='tab3'>tab3</TabItem>
            </Tabs>
        </>
    )
}
let wrapper: RenderResult, activeElemnt: HTMLElement, disabledElement: HTMLElement, tabElement: HTMLElement
describe('tabs 测试', () => {
    beforeEach(() => {
        wrapper = render(generateTabs(testTabsProps))
        activeElemnt = wrapper.getByText('tab1');
        disabledElement = wrapper.getByText('tab2');
        tabElement = wrapper.getByTestId('test-tabs');
    })
    it('tabs 传参', () => {
        expect(tabElement).toBeInTheDocument()
        expect(tabElement).toHaveClass('tabs testtabs')
        expect(activeElemnt).toHaveClass('tab-item tab-item-active')
        expect(disabledElement).toHaveClass('tab-item tab-item-disabled')

    });
    it('tabs 回调', () => {
        let tabsItemClickElement = wrapper.getByText('tab3')
        expect(tabsItemClickElement).toBeInTheDocument()
        fireEvent.click(tabsItemClickElement)
        expect(tabsItemClickElement).toHaveClass('tab-item tab-item-active')
        expect(activeElemnt).not.toHaveClass('tab-item tab-item-active')
        fireEvent.click(disabledElement)
        expect(disabledElement).not.toHaveClass('tab-item-active')
        expect(disabledElement).toHaveClass('tab-item tab-item-disabled')
    });
})
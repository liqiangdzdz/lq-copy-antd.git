import { storiesOf } from "@storybook/react";
import React from "react";
import { TabItem } from "./TabItem";
import { Tabs } from "./Tabs";
const T = () => (
    <Tabs defaltIndex='0'>
        <TabItem title='tab1'>
            <h1>tab1</h1>
        </TabItem>
        <TabItem disabled={true} title='tab2'>tab2</TabItem>
        <TabItem title='tab3'>
            <h1>tab3</h1>
        </TabItem>
    </Tabs>
)
storiesOf('Tabs 标签页', module)
    .add('Tabs', T)
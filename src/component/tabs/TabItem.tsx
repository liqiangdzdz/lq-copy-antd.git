import classNames from "classnames";
import React, { ReactNode, useContext } from "react";
import { tabsContext } from "./Tabs";

export interface ITabItemProps {
      /** 高亮 */
    index?: string;
      /** 禁用 */
    disabled?: boolean;
      /** 卡片名 */
    title: string;
    children?: ReactNode;
    style?: React.CSSProperties;
    className?: string
}


export const TabItem: React.FC<ITabItemProps> = (props: ITabItemProps) => {
    const { index, disabled, title, children, style, className } = props
    const { defaltIndex, tabsOnSelect } = useContext(tabsContext)
    const tabItemClasNames = classNames('tab-item', className, {
        'tab-item-disabled': disabled,
        'tab-item-active': index === defaltIndex
    })
    const tabItemHeade = () => {
        if (tabsOnSelect && !disabled) {
            tabsOnSelect(index ? index : '0', children)
        }
    }
    return (
        <>
            <li style={style} className={tabItemClasNames} onClick={tabItemHeade}>
                {title}
            </li>
        </>
    )
}

TabItem.displayName = 'TabItem'
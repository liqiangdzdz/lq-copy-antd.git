import React from "react";
import { CSSTransition } from "react-transition-group";
import { CSSTransitionProps } from "react-transition-group/CSSTransition";

type Animition = 'tran-in-top' | 'tran-in-left' | 'tran-in-bottom' | 'tran-in-right'

// &：俩个接口都拥有的类型
type ITransitionProps = CSSTransitionProps & {
    animitionName: Animition
    wrapper?: boolean
}


export const Transition: React.FC<ITransitionProps> = (props: ITransitionProps) => {
    const { children, classNames, animitionName, wrapper, ...restProps } = props
    return (
        <>
            <CSSTransition classNames={classNames ? classNames : animitionName} {...restProps}>{
                wrapper ? <>{children}</> : children
            }</CSSTransition>
        </>
    )
}

Transition.defaultProps = {
    unmountOnExit: true,
    appear: true
}

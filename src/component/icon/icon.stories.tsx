import { storiesOf } from "@storybook/react";
import React from "react";
import { Icon } from "./Icon";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)
const icons = () => (
    <Icon icon={"coffee"} size='10x' theme='primary'></Icon>
)

storiesOf('icon 图标', module)
    .add('Icon', icons)
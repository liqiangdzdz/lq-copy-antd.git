import React, { useState } from "react";
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import classNames from "classnames";

export type ThemeProps = 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'light' | 'dark'

export interface IIconProps extends FontAwesomeIconProps {
    /**选择种类 */
    theme?: ThemeProps
}
/**
 * 可用于美化页面
 * 
 * ### 引用方法
 * 
 * ~~~js
 * import { Icon } from "./Icon";
 *import { library } from '@fortawesome/fontawesome-svg-core'
 *import { fas } from '@fortawesome/free-solid-svg-icons'
 * ~~~
 */
export const Icon: React.FC<IIconProps> = (props: IIconProps) => {
    const { className, theme, ...restProps } = props
    const iconClassNames = classNames('icon', className, {
        // 只要有theme那就添加icon-${theme}
        [`icon-${theme}`]: theme
    })
    return (
        <>
            <FontAwesomeIcon className={iconClassNames} {...restProps} />
        </>
    )
}

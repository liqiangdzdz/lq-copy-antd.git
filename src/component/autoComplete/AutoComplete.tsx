import classNames from "classnames";
import React, { ChangeEvent, KeyboardEvent, ReactElement, useEffect, useRef, useState } from "react";
import useClickOutside from "../../hooks/useClickOutside";
import useDebounce from "../../hooks/useDebounce";
import { Icon } from "../icon/Icon";
import { IInputProps, Input } from "../input/Input";

export interface IAutoCompleteProps extends Omit<IInputProps, 'onSelect'> {
    fetchSuggestions: (keyWord: string) => IStrAndObjListType[] | Promise<IStrAndObjListType[]>;
    onSelect?: (value: IStrAndObjListType) => void;
    renderOption?: (value: IStrAndObjListType) => ReactElement
}

export interface IPromptListObject {
    value: string
}

export type IStrAndObjListType<T = {}> = T & IPromptListObject

export const AutoComplete: React.FC<IAutoCompleteProps> = (props: IAutoCompleteProps) => {
    const { fetchSuggestions, value, onSelect, renderOption, ...restProps } = props
    const [values, setValue] = useState(value as string)
    const [promptList, setPromptList] = useState<IStrAndObjListType[]>([])
    const [loading, setLoading] = useState(false)
    const [highlightIndex, setHighlighIndex] = useState(-1)
    const tiggerSearch = useRef(false)
    const domRef = useRef<HTMLDivElement>(null)
    const debouncevalue = useDebounce(values, 500)
    useClickOutside(domRef, () => {
        setPromptList([])
    })
    useEffect(() => {
        asyncHronousLoading()
    }, [debouncevalue])


    const onAutoInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        let val = e.target.value.trim()
        setValue(val)
        tiggerSearch.current = true

    }

    const highlight = (index: number) => {
        if (index < 0) index = 0
        if (index >= promptList.length) {
            index = promptList.length - 1
        }
        setHighlighIndex(index)
    }
    const onAutoInputKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {

        switch (e.keyCode) {
            case 13:
                if (promptList[highlightIndex]) {
                    callBack(promptList[highlightIndex])
                }
                break;
            case 38:
                highlight(highlightIndex - 1)
                break;
            case 40:
                highlight(highlightIndex + 1)
                break;
            case 27:
                setPromptList([])
                break;
            default:
                break;
        }
    }

    const asyncHronousLoading = async () => {
        if (debouncevalue && tiggerSearch.current) {
            let res = fetchSuggestions(debouncevalue)

            if (res instanceof Promise) {
                setLoading(true)
                let data = await res
                setPromptList(data)
                setLoading(false)
            } else {
                setPromptList(res)
            }
        } else {
            setPromptList([])
        }

        setHighlighIndex(-1)
    }

    const callBack = (item: IStrAndObjListType) => {
        setValue(item.value)
        setPromptList([])
        if (onSelect) {
            onSelect(item)
        }
        tiggerSearch.current = false
    }

    const createPromptReactElement = (item: IStrAndObjListType) => {
        if (renderOption) {
            return renderOption(item)
        }
        return item.value
    }

    const generateDrodown = () => {
        return (
            <>
                <ul className="suggestion-list">
                    {
                        promptList.map((item, index) => {
                            console.log(index, highlightIndex, index === highlightIndex);

                            const names = classNames('suggestion-item', {
                                'is-active': index === highlightIndex
                            })
                            return (
                                <li key={index} onClick={() => { callBack(item) }} className={names}>
                                    {
                                        createPromptReactElement(item)
                                    }
                                </li>
                            )
                        })
                    }
                </ul>
            </>
        )
    }

    return (
        <div className="auto-complete" ref={domRef}>
            <Input value={values} onChange={onAutoInputChange} onKeyDown={onAutoInputKeyDown}></Input>
            {
                loading && <ul><Icon icon={'spinner'} spin></Icon></ul>
            }
            {
                (promptList.length > 0) && generateDrodown()
            }
        </div>
    )
}



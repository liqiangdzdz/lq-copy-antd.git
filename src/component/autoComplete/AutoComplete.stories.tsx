import { action } from "@storybook/addon-actions";
import { storiesOf } from "@storybook/react";
import React from "react";
import { AutoComplete, IStrAndObjListType } from "./AutoComplete";

const arr = ['html', 'css', 'javascript', 'typescript', 'jquery', 'vue', 'react', 'angular', 'webpack', 'java', 'c', 'c#', 'c++', 'php', 'go', 'rust']
const arr2 = [{ value: 'html' }, { value: 'css' }, { value: 'javascript' }]


const fetchSuggestions = (keyWord: string) => {
    return arr.filter(item => item.includes(keyWord)).map(i => ({ value: i }))

}
const fetchSuggestionsPromise = async (keyWord: string) => {
    const res = await fetch(`https://api.github.com/search/users?q=${keyWord}`)
    const {items} = await res.json()
    let arrP = items.slice(0, 10).map((item: any) => ({ value: item.login, ...item}))
    return arrP
}
const option = (item: IStrAndObjListType) => {
    return (
        <>
            <h1>{item.value}</h1>
        </>
    )
}

const auto = () => {
    return (
        <>
            <div style={{ width: '500px' }}>
                <AutoComplete fetchSuggestions={fetchSuggestions} onSelect={action('selected')} renderOption={option} />
            </div>
        </>
    )
}

const autoPromise = () => {
    return (
        <>
            <div style={{ width: '500px' }}>
                <AutoComplete fetchSuggestions={fetchSuggestionsPromise} onSelect={action('selected')} renderOption={option} />
            </div>
        </>
    )
}


storiesOf('AutoComplete 自动提示', module)
    .add('AutoComplete', auto)
    .add('异步 AutoComplete', autoPromise)
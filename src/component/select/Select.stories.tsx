import { storiesOf } from "@storybook/react";
import React from "react";
import { ISelectListObjType, Select } from "./Select";

const datas: ISelectListObjType[] = [
    { value: 'a' },
    { value: 'b' },
    { value: 'c' },
    { value: 'd' },
    { value: 'e' },
]

const back = (val:string[]) => {
    console.log('val:::',val);
    
}

const selct = () => {
    return (<Select data={datas} callback={back}></Select>)
}

storiesOf('select 下拉框', module)
    .add('Select', selct)
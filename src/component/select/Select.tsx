import classNames from "classnames";
import React, { FC, useState,useRef} from "react";
import useClickOutside from "../../hooks/useClickOutside";
import { Input } from "../input/Input";

export interface ISelectProps {
    /** 下拉弹框的数据:[{value:string,...rest}]*/
    data: ISelectListObjType[];
    /** 选中值的回调 返回：string[]*/
    callback?: (val: string[]) => void
}

export interface ISelectListObj {
    value: string
}

export type ISelectListObjType<T = {}> = T & ISelectListObj

/***
 * 弹出一个下拉菜单给用户选择操作，用于代替原生的选择器，或者需要一个更优雅的多选器时 
 * 
 * ### 引用方式
 * ~~~js
 * import { Select } from "./Select";
 * ~~~
 * 
 */

export const Select: FC<ISelectProps> = (props: ISelectProps) => {
    const { data, callback } = props

    const [selectList, setSelectList] = useState<ISelectListObjType[]>(data)
    const [valueList, setValueList] = useState<string[]>([])
    const [focusFlag, setFocusFlag] = useState(false)
    const domRef = useRef<HTMLDivElement>(null)
    useClickOutside(domRef,() => {
        setFocusFlag(false)
    })

    const onItemClick = (val: string) => {
        const index = valueList.indexOf(val)
        if (index === -1) {
            setValueList([...valueList, val])
            if (callback) {
                callback(valueList)
            }
        }else{
            deleteValueItem(index)
        }
    }

    const deleteValueItem = (index: number) => {
        let arr = valueList.filter((i, countIndex) => {
            return index !== countIndex
        })
        setValueList(arr)
        if (callback) {
            callback(valueList)
        }
    }
    const getvaluelist = () => {
        return (
            <div className="value-item">
                {
                    valueList.map((i, index) => {
                        return (<div key={index} className={'value-item-item'} onClick={() => { deleteValueItem(index) }}>{i}</div>)
                    })
                }
            </div>
        )

    }
    const highlight = (val:string) => {
        const index = valueList.indexOf(val)
        if (index === -1) {
           return false
        }
        return true
    }
    const generateDrodown = () => {
        return (
            <ul className="suggestion-list">
                {
                    selectList.map((i, index) => {
                        const names = classNames('suggestion-item', {
                            'is-active':highlight(i.value)
                        })
                        return (<li key={index} onClick={() => { onItemClick(i.value) }} className={names}>{i.value}</li>)
                    })
                }
            </ul>
        )
    }

    return (
        <>
            <div className="select-complete" ref={domRef}>
                {
                  focusFlag &&   getvaluelist()
                }
                <Input readOnly onFocus={() => { setFocusFlag(true) }}></Input>
                {
                 focusFlag && generateDrodown()
                }
            </div>
        </>
    )
}
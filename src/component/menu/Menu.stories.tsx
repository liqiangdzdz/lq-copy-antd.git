import { storiesOf } from "@storybook/react";
import React from "react";
import { Menu } from "./Menu";
import { MenuItem } from "./MenuItem";
import { SubMenu } from "./SubMenu";


const M = () => (
    <Menu mode='vertical' defaultIndex='0' onSelect={(index: string) => { console.log('index:', index) }} defaultOpenSubMenus={['3']}>
        <MenuItem>menuItem-1</MenuItem>
        <MenuItem >menuItem-2</MenuItem>
        <MenuItem >menuItem-3</MenuItem>
        <SubMenu title={'submenu 1'}>
            <MenuItem >submenuItem-1</MenuItem>
            <MenuItem >submenuItem-2</MenuItem>
            <MenuItem >submenuItem-3</MenuItem>
        </SubMenu>
    </Menu>
)
storiesOf('Menu 侧边栏', module)
    .add('Menu', M)
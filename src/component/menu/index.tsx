import { FC } from 'react'
import { IMenuProps, Menu } from './Menu'
import { IMenuItemProps, MenuItem } from './MenuItem'
import { ISubMenuProps, SubMenu } from './SubMenu'

export type IMenuComponent = FC<IMenuProps> & {
    Item: FC<IMenuItemProps>,
    SubMenu: FC<ISubMenuProps>
}
const TransMenu = Menu as IMenuComponent

TransMenu.Item = MenuItem
TransMenu.SubMenu = SubMenu

export default TransMenu
menu:
1.defaultIndex：控制高亮
2.onSelect（selectIndex）：回调函数，获得所在的menuItem
3.mode：控制竖向排列还是横向排列

menuItem：
1.index:当前Item所在的key
2.disabled：不可点击

1、需将父组件的defaultIndex传给子组件
2、下拉菜单第一步：先全部显示出来
           第二步：使用display来控制显示与隐藏
           纵向点击显示，横向鼠标经过显示
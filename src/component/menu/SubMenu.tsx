import classNames from "classnames";
import React, { ReactNode, useContext, useState } from "react";
import { Icon } from "../icon/Icon";
import { MenuContext } from "./Menu";
import { IMenuItemProps } from "./MenuItem";
import { Transition } from "../transition/Transition";

export interface ISubMenuProps {
    /** 高亮*/
    index?: string;
    /** subMenu的标题*/
    title: string;
    className?: string;
    style?: React.CSSProperties;
    children?: ReactNode
}

export const SubMenu: React.FC<ISubMenuProps> = (props: ISubMenuProps) => {
    const { index, title, className, style, children } = props
    const { defaultIndex, mode, defaultOpenSubMenus } = useContext(MenuContext)
    const openSubMenus = defaultOpenSubMenus as Array<string>
    const isOpen = (index && mode === 'vertical') ? openSubMenus.includes(index) : false
    const [menuShow, setMenuShow] = useState(isOpen)
    const subMenuClassNames = classNames('menu-item submenu-item', className, {
        'submenu-active': defaultIndex.slice(0, 1) === index,
        'icon-active': menuShow
    })
    // 控制高亮
    const handeleClick = (e: React.MouseEvent) => {
        e.preventDefault();
        setMenuShow(!menuShow);
    }
    const handeleMouse = (e: React.MouseEvent, flage: boolean) => {
        e.preventDefault();
        let timer;
        clearTimeout(timer)
        timer = setTimeout(() => {
            setMenuShow(flage)
        }, 500);

    }

    // 根据横向还是纵向来决定下拉方式
    const mouseEvent = mode === 'transverse' ? {
        onMouseEnter: (e: React.MouseEvent) => { handeleMouse(e, true) },
        onMouseLeave: (e: React.MouseEvent) => { handeleMouse(e, false) }

    } : {}
    const clickEvent = mode === 'vertical' ? {
        onClick: handeleClick
    } : {}
    const renderChildren = () => {
        const classess = classNames('submenu', {
            'menu-show': menuShow
        })
        return (
            <>
                <Transition in={menuShow} timeout={1000} animitionName={'tran-in-right'} >
                    <ul className={classess}>
                        {
                            React.Children.map(children, (child, i) => {
                                const childrenElement = child as React.FunctionComponentElement<IMenuItemProps>
                                if (childrenElement.type.displayName === 'MenuItem') {
                                    return React.cloneElement(childrenElement, {
                                        index: `${index}-${i}`
                                    })
                                }
                                console.error('submenu里面只能是menuitem')
                            })
                        }
                    </ul>
                </Transition>
            </>
        )
    }
    return (
        <>
            {/* mouse放div会导致鼠标移到mouseItem的时候触发 onMouseLeave*/}
            <li key={index} className={subMenuClassNames} {...mouseEvent}>
                <div className="submenu-title" {...clickEvent}>
                    {title}
                    <Icon icon={'angle-down'} className={'arrow-icon'} />
                </div>
                {renderChildren()}
            </li>
        </>
    )
}

SubMenu.displayName = 'SubMenu'
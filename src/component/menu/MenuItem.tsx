import classNames from 'classnames';
import React, { ReactNode, useContext } from 'react'
import { MenuContext } from './Menu';

export interface IMenuItemProps {
    /** 对应高亮*/
    index?: string;
    /** 禁用*/
    disabled?: boolean;
    children?: ReactNode;
    style?: React.CSSProperties;
    className?: string
}

export const MenuItem: React.FC<IMenuItemProps> = (props: IMenuItemProps) => {
    const { index, disabled, children, style, className } = props
    const {defaultIndex,menuCallBack} = useContext(MenuContext)
    const MenuItemClassNames = classNames('menu-item',className,{
        'menu-item-disabled':disabled,
        'menu-item-active':index === defaultIndex && !disabled
    })

    const MenuItemIndexClick = () => {
        if(menuCallBack && !disabled){
            
            menuCallBack(index ? index:'0')
        }
    }

    return (
        <>
            <li className={MenuItemClassNames} onClick={MenuItemIndexClick} style={style}>
                {
                    children
                }
            </li>
        </>
    )
}
MenuItem.displayName = 'MenuItem'
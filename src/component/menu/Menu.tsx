import classNames from "classnames";
import React, { cloneElement, createContext, ReactNode, useState } from "react";
import { IMenuItemProps } from "./MenuItem";

export type MenuCallBack = (index: string) => void
export type MenuMode = 'vertical' | 'transverse'

export interface IMenuProps {
    /** 控制高亮*/
    defaultIndex?: string;
    /** 点击回调*/
    onSelect?: MenuCallBack;
    /** 纵向还是横向*/
    mode: MenuMode;
    children?: ReactNode;
    className?: string;
    style?: React.CSSProperties;
    /** 默认展开*/
    defaultOpenSubMenus?: string[]
}

export interface IMenuContext {
    defaultIndex: string;
    menuCallBack?: MenuCallBack;
    mode?: MenuMode;
    defaultOpenSubMenus?: string[]
}

export const MenuContext = createContext<IMenuContext>({ defaultIndex: '0' })

/** 
 * 导航菜单是一个网站的灵魂，用户依赖导航在各个页面中进行跳转。一般分为顶部导航和侧边导航，顶部导航提供全局性的类目和功能，侧边导航提供多级结构来收纳和排列网站架构。
 * 
 * ### 引用方法
 * 
 * ~~~js
 * import { Menu } from "./Menu";
 *import { MenuItem } from "./MenuItem";
 *import { SubMenu } from "./SubMenu";
 * ~~~
 * 
*/
export const Menu: React.FC<IMenuProps> = (props: IMenuProps) => {
    const { defaultIndex, onSelect, mode, className, style, children, defaultOpenSubMenus } = props
    const [menuIndex, setMenuIndex] = useState(defaultIndex)
    const MenuClassNames = classNames('menu', className, {
        'menu-vertical': mode === 'vertical',
        'menu-transverse': mode === 'transverse'
    })
    const menuCallBack = (index: string) => {
        setMenuIndex(index)
        if (onSelect) {
            onSelect(index ? index : '0')
        }
    }

    const MenuContextValue: IMenuContext = {
        defaultIndex: menuIndex ? menuIndex : '0',
        menuCallBack,
        mode,
        defaultOpenSubMenus
    }

    // 控制children的内容
    const renderChildren = () => {
        // https://zh-hans.reactjs.org/docs/react-api.html#reactchildren
        return React.Children.map(children, (child, i) => {
            //FC:函数式组件 FunctionComponentElement:返回的元素，即FC的实列
            const childElement = child as React.FunctionComponentElement<IMenuItemProps>
            if (childElement.type.displayName === 'MenuItem' || childElement.type.displayName === 'SubMenu') {
                // https://zh-hans.reactjs.org/docs/react-api.html#cloneelement
                return cloneElement(childElement, {
                    index: `${i}`
                })
            }
            console.error('menu内部只能放menuitem或者submenu')
        })
    }

    return (
        <>
            <MenuContext.Provider value={MenuContextValue}>
                <ul className={MenuClassNames} style={style} data-testid='test-menu'>
                    {
                        renderChildren()
                    }
                </ul>
            </MenuContext.Provider>
        </>
    )
}

Menu.displayName = 'Menu'
Menu.defaultProps = {
    defaultOpenSubMenus: [],
}
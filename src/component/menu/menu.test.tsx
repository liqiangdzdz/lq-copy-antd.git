import { fireEvent, render, RenderResult, waitFor } from "@testing-library/react";
import React from "react";
import { IMenuProps, Menu } from "./Menu";
import { MenuItem } from "./MenuItem";
import { SubMenu } from "./SubMenu";

const testMenuProps: IMenuProps = {
    defaultIndex: '0',
    mode: 'vertical',
    onSelect: jest.fn(),
    className: 'testmenu'
}

const generateMenu = (props: IMenuProps) => {
    return (
        <Menu {...props} defaultIndex='2' mode='transverse'>
            <MenuItem >menuitem0</MenuItem>
            <MenuItem disabled={true}>menuitem1</MenuItem>
            <MenuItem >menuitem2</MenuItem>
            <SubMenu title="dropdown">
                <MenuItem>
                    drop1
                </MenuItem>
            </SubMenu>
            <SubMenu title="opened">
                <MenuItem>
                    opened1
                </MenuItem>
            </SubMenu>
        </Menu>
    )
}

let wrapper: RenderResult, menuElemet: HTMLElement, disabledElement: HTMLElement, activeElement: HTMLElement;

// 添加css
const createStyle = () => {
    const css: string = `
    .submenu {
      display: none;
    }
    .submenu .menu-show {
      display:block;
    }
    .submenu-title {
        display: flex;
        align-items: center;
      }
  `
    const style = document.createElement('style')
    style.type = 'text/css'
    style.innerHTML = css
    return style
}

describe('测试menu', () => {
    beforeEach(() => {
        wrapper = render(generateMenu(testMenuProps))
        //  wrapper.container 这个是htmlElement
        wrapper.container.append(createStyle())
        menuElemet = wrapper.getByTestId('test-menu')
        disabledElement = wrapper.getByText('menuitem1')
        activeElement = wrapper.getByText('menuitem2')
    });
    it('测试menu的传参', () => {
        // menu是否在文档中
        expect(menuElemet).toBeInTheDocument()
        // 是否存在对应的class
        expect(menuElemet).toHaveClass('menu testmenu')
        // 查看里面渲染的子组件有几个
        expect(menuElemet.getElementsByTagName('li').length).toBe(7)
        expect(menuElemet.querySelectorAll(':scope > li').length).toBe(5)
        // 判断是否对应active和disabled
        expect(activeElement).toHaveClass('menu-item menu-item-active')
        expect(disabledElement).toHaveClass('menu-item menu-item-disabled')

    });
    it('测试menuItem的点击事件', () => {
        const clickItemElement = wrapper.getByText('menuitem0')
        // 点击menuitem0
        fireEvent.click(clickItemElement)
        expect(clickItemElement).toHaveClass('menu-item menu-item-active')
        expect(activeElement).not.toHaveClass('menu-item menu-item-active')
        // 查看调用的函数的所传的参数
        expect(testMenuProps.onSelect).toHaveBeenCalledWith('0')
        // 查看disabled
        fireEvent.click(disabledElement)
        expect(disabledElement).not.toHaveClass('menu-item-active')
        expect(testMenuProps.onSelect).not.toHaveBeenCalledWith('1')
    });
    it('测试menu的mode', () => {
        expect(menuElemet).toHaveClass('menu-transverse')
        
    });
    it('测试subMenu', async() => {
        // wrapper.queryByText:返回elemet或者null
        // toBeVisible:是否存在
        expect(wrapper.queryByText('drop1')).not.toBeVisible()
        const dropdownElement = wrapper.getByText('dropdown')
        fireEvent.mouseEnter(dropdownElement)
        await waitFor(() => {
          expect(wrapper.queryByText('drop1')).not.toBeVisible()
        })
        

    });
})
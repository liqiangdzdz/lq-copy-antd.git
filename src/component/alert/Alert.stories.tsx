import { storiesOf } from "@storybook/react";
import React from "react";
import { Altert, IAlertDisplay, IAlertType } from "./Altert";

const successAltert = () => (
    <>
        <Altert display={IAlertDisplay.Block} alertType={IAlertType.Success} message='成功'></Altert>
    </>
)
const errorAltert = () => (
    <>
        <Altert display={IAlertDisplay.Block} alertType={IAlertType.Error} message='失败'></Altert>
    </>
)
const waringAltert = () => (
    <>
        <Altert display={IAlertDisplay.Block} alertType={IAlertType.Waring} message='警告'></Altert>
    </>
)
const infoAltert = () => (
    <>
        <Altert display={IAlertDisplay.Block} alertType={IAlertType.Info} message='自定义'></Altert>
    </>
)

storiesOf('Alert 提示框', module)
    .add('Altert', successAltert)
    .add('error Alert', errorAltert)
    .add('waring Alert', waringAltert)
    .add('info Alert', infoAltert)
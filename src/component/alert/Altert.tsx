import classNames from "classnames";
import React, { ReactNode } from "react";

export enum IAlertText {
    Y = 'yes',
    N = 'No'
}

export enum IAlertType {
    Success = 'success',
    Error = 'error',
    Waring = 'waring',
    Info = "info"
}
export enum IAlertDisplay {
    Block = "inline-block",
    None = "none"
}

interface IProps {
    /** 禁用 */
    display: IAlertDisplay
    className?: string
    /** 种类 */
    alertType: IAlertType
    children?: ReactNode
    /** 显示文本*/
    message: string
    /** 自定义内容，只能info使用*/
    description?: string
    style?: {}
    /** 关闭回调函数*/
    close?: () => void
}
/**
 * 
 * 页面中最常用的组件，适用于提示用户
 * ### 引用方法
 * ~~~js
 * iimport { Altert, IAlertDisplay, IAlertType } from './component/alert/Altert';
 * ~~~
 */
export const Altert: React.FC<IProps> = (props: IProps) => {
    const { className, alertType, display, children, message, close, style, description } = props
    const alertClassNames = classNames('alert', className, {
        [`alert-${alertType}`]: alertType
    })
    return (
        <>
            <div className={alertClassNames} style={{ display: display, ...style }}>
                <span className={description ? "alert-title" : ""}>
                    {message}
                </span>
                <span className={description ? "alert-text" : ""}>
                    {description}
                </span>
                <span className="alert-close" onClick={close}>X</span>
            </div>
        </>
    )



}

Altert.defaultProps = {
    message: 'success'
}
import { render } from "@testing-library/react";
import React from "react";
import { Altert, IAlertDisplay, IAlertType } from "./Altert";

describe('test alert component', () => {
    it('success alert', () => {
        const successAlert = render(<Altert alertType={IAlertType.Success} message='成功' display={IAlertDisplay.Block}>Nice</Altert>)
        const element = successAlert.queryByText('Nice')
        expect(element).toBeInTheDocument()
    })
    it('props alert', () => {
        const propsAlert = render(<Altert display={IAlertDisplay.Block} alertType={IAlertType.Success} message="成功" className="alt">Nice</Altert>)
        const element = propsAlert.queryByText('Nice')
        expect(element).toBeInTheDocument()
        // expect(e).toHaveClass('alert alt alert-success')
    })
})
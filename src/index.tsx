// import React from 'react';
// import ReactDOM from 'react-dom';
// import './style/index.scss';
// import App from './App';
// import reportWebVitals from './reportWebVitals';

// ReactDOM.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
//   document.getElementById('root')
// );
// reportWebVitals();
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
library.add(fas)
// import Button from './component/button';
// export default Button;
export {default as Button} from './component/button'
export {default as Alert} from './component/alert'
export {default as Menu} from './component/menu'
export {default as Tabs} from './component/tabs'
export {default as AutoComplete} from './component/autoComplete'
export {default as Icon} from './component/icon'
export {default as Input} from './component/input'
export {default as Progress} from './component/progress'
export {default as Select} from './component/select'
export {default as Upload} from './component/upload'

 
import '../src/style/index.scss'
import React from "react";
import { addDecorator, addParameters, StoryContext } from '@storybook/react';
import { withInfo } from '@storybook/addon-info'
const styles: React.CSSProperties = {
  textAlign: 'center'
}
export const centerDecorator = (storyFn: any) => (
  <div style={styles}>
    <h3>组件演示</h3>
    {storyFn()}
  </div>
)
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
addDecorator(centerDecorator)
addDecorator(withInfo)
addParameters({
    info: {
      inline: true,
      header: false
    }
  })
